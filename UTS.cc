/** @author Inka Aqila N
* @NIM 301230043
* @version 20 November 2023
* @UTS Algoritma dan Pemrograman
*/

#include <iostream>
#include <stdlib.h>

using namespace std;

int main() {

    system("clear");

    int x = 0;
    int y = 0;
    char OperatorAritmatika;

    cout<<"Masukkan Bilangan ke-1 : "; cin>>x;
    cout<<"Masukkan Operator (+, -, *, /) : "; cin>>OperatorAritmatika;
    cout<<"Masukkan Bilangan ke-2 : "; cin>>y;

    switch (OperatorAritmatika) {
        case '+':
        cout<<"Bilangan ke-1 + Bilangan ke-2 = "<<x<<" + "<<y<<" = "<<x+y<<endl;
        break;
        case '-':
        cout<<"Bilangan ke-1 - Bilangan ke-2 = "<<x<<" - "<<y<<" = "<<x-y<<endl;
        break;
        case '*':
        cout<<"Bilangan ke-1 * Bilangan ke-2 = "<<x<<" * "<<y<<" = "<<x*y<<endl;
        break;
        case '/':
        cout<<"Bilangan ke-1 / Bilangan ke-2 = "<<x<<" / "<<y<<" = "<<x/y<<endl;
        break;
        default:
        cout<<"Operator Tidak Valid";
    }

    return 0;
}
