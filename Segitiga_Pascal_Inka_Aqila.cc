#include <iostream>
using namespace std;

int faktorial(int n) {
    if (n == 0 || n == 1) {
        return 1;
    } else {
        return n * faktorial(n - 1);
    }
}

int main() {
    int a, i, j;

    cout << "Masukkan Nilai : ";
    cin >> a;

    for (i = 0; i < a; i++) {
        for (j = 0; j < a - i - 1; j++) {
            cout << " ";
        }

        for (j = 0; j <= i; j++
        
        ) {
            cout << faktorial(i) / (faktorial(j) * faktorial(i - j)) << " ";
        }

        cout << endl;
    }

    return 0;
}
